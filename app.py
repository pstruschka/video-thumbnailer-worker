#!/usr/bin/env python3
import os
import logging
import subprocess
import uuid
import time
import hashlib
import bson
import requests
import pika


LOG = logging

RABBIT_HOST = os.getenv('RABBIT_HOST', 'localhost')
RABBIT_QUEUE = os.getenv('RABBIT_QUEUE', 'make_thumbnail')
SOS_HOST = os.getenv('SOS_HOST', 'localhost:5000')
SOS_BASE_URL = 'http://{}'.format(SOS_HOST)

INSTANCE_NAME = uuid.uuid4().hex
ETAG = str()
VIDEO_FILE = 'video.mp4'
GIF_FILE = 'thumbnail.gif'

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(name)-32s - %(levelname)-8s - %(message)-s'
)

def get_object(logger, bucket_name, object_name, etag):
    global ETAG
    if etag == ETAG:
        logger.info('%s already exists as %s', object_name, VIDEO_FILE)
        return
    logger.info('get_object %s/%s', bucket_name, object_name)
    request_url = '{}/{}/{}'.format(SOS_BASE_URL, bucket_name, object_name)
    logger.info('request url %s', request_url)
    resp = requests.get(request_url, stream=True)
    logger.info('get_object: %r', resp.status_code)
    if resp.status_code != requests.codes['OK']:
        resp.close()
        resp.raise_for_status()

    with open(VIDEO_FILE, 'wb') as f:
        for chunk in resp.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    ETAG = resp.headers.get('Etag')
    resp.close()
    logger.info('%s saved as %s', object_name, VIDEO_FILE)


def post_object(logger, bucket_name, object_name):
    logger.info("post_object %s/%s", bucket_name, object_name)

    hash_md5 = hashlib.md5()

    create_object_location = '{}/{}/{}'.format(SOS_BASE_URL, bucket_name, object_name)

    resp = requests.post(create_object_location + '?create')
    if resp.status_code != requests.codes['OK']:
        logger.warn('object may already exist, attempting deletion')
        resp = requests.delete(create_object_location + '?delete')
        if resp.status_code != requests.codes['OK']:
            logger.warn('Could not be deleted, quitting')
            return
        else:
            resp = requests.post(create_object_location + '?create')
            if resp.status_code != requests.codes['OK']:
                logger.warn('Could not be created, quitting')
                return

    with open(GIF_FILE, 'rb') as f:
        data = f.read()
        hash_md5.update(data)
        resp = requests.put(url=create_object_location + '?partNumber=1',
                            data=data,
                            headers={'Content-Type': 'application/octet-stream',
                                     'Content-MD5': hash_md5.hexdigest()})
        logger.debug(resp.content)

    resp = requests.post(create_object_location + '?complete')
    if resp.status_code != requests.codes['OK']:
        logger.warn('object could not be completed')



# download the image, call subprocess to generate gif, upload image
def run_task(logger, task):
    logger.info('Task %r', task)
    bucket_name = task.get('src_bucket')
    object_name = task.get('src_object')
    output_bucket_name = task.get('dst_bucket')
    output_object_name = task.get('dst_object')
    scenes = str(task.get('scenes'))
    length = str(task.get('length'))
    text = task.get('text')
    e_tag = task.get('eTag')
    #text = ['"' + t + '"' for t in text]
    logger.info('%s', str(text))

    try:
        get_object(logger, bucket_name, object_name, e_tag)
    except requests.ConnectionError:
        logger.error('ConnectionError')
    except requests.HTTPError:
        logger.error('RequestError')

    logger.info("Starting 'make_thumbnail' Subprocess")
    status = subprocess.call(['make_thumbnail', VIDEO_FILE, GIF_FILE, scenes, length, *text])
    logger.info('subprocces ran with %d', status)

    # Upload the final gif
    logger.info('out: %s, %s', output_bucket_name, output_object_name)
    post_object(logger, output_bucket_name, output_object_name)


def handle_message(logger, channel, method, _properties, body):
    logger.info('Received %r', body)
    try:
        task = bson.loads(body)
    except bson.struct.error:
        logger.warn('Invalid BSON')
        #channel.basic_ack(delivery_tag=method.delivery_tag)
        return
    run_task(logger, task)
    logger.info('Done')
    #channel.basic_ack(delivery_tag=method.delivery_tag)


def main():
    LOG.debug('ENVIRONMENT - RABBIT_HOST: %s, SOS_HOST: %s', RABBIT_HOST, SOS_HOST)
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)

    LOG.getLogger("pika").setLevel(logging.WARNING)
    named_logging = LOG.getLogger(name=INSTANCE_NAME)

    named_logging.info('Connect to RabbitMQ')
    retries = 3
    rabbit_conn = None

    while rabbit_conn is None and retries > 0:
        try:
            rabbit_conn = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_HOST))
        except pika.exceptions.ConnectionClosed:
            retries -= 1
            rabbit_conn = None
            named_logging.warning('RabbitMQ connection failed, %d retries left', retries)
            time.sleep(5)
    if rabbit_conn is None:
        named_logging.critical('No more retries - KILLING SELF')
        exit(1)
    named_logging.info('RabbitMQ connection established')

    channel = rabbit_conn.channel()

    channel.queue_declare(queue=RABBIT_QUEUE, durable=True)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(
        lambda ch, method, properties, body:
        handle_message(named_logging, ch, method, properties, body),
        queue=RABBIT_QUEUE, consumer_tag=INSTANCE_NAME,
        no_ack=True)

    named_logging.info('Ready to consume tasks')
    channel.start_consuming()

if __name__ == '__main__':
    main()
