#!/usr/bin/env bash

die () {
    echo >&2 "$@"
    exit 1
}


[ $# -ge 4 ] || die "4 arguments required, $# provided"

[ -f "$1" ] || die "File $2 does not exist"

echo $3 | grep -E -q '^[0-9]+$' || die "numeric argument required, $3 provided"

echo $4 | grep -E -q '^[0-9]+$' || die "numeric argument required, $4 provided"

inputVidFile=$1
outputGifFile=$2
numScenes=$3
gifLength=$4

echo "$@"

counter=1
index=1
textArr=()

for i in "$@";do
    if [[ $counter -ge 5 ]]; then
	textArr[$index]=${i}
	((index++))
    fi
    ((counter++))
done

seconds=`ffprobe -v quiet -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$inputVidFile"`
step=`echo "scale=2; $seconds/($numScenes+1.0)" | bc`

t=`echo "scale=2; $gifLength/$numScenes" | bc`

for i in `seq $numScenes`;do
    s=`echo "scale=2; $step*$i" | bc`
    `ffmpeg -v quiet -ss "$s" -i "$inputVidFile" -t "$t"\
    -vf "scale=320:240:flags=lanczos,fps=10, \
    drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf:text='${textArr[$i]}':\
    fontcolor=white:fontsize=28:box=1:boxcolor=black@0.5:x=(w-text_w)/2:y=((h-text_h)/4)*3" \
    -threads 8 frame"$i"%03d.png`
done

`convert -loop 0 frame*.png "$outputGifFile"`
`chmod 666 "$outputGifFile"`

`rm frame*.png`
exit 0

