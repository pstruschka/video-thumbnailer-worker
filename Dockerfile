FROM acleancoder/imagemagick-full:latest


RUN add-apt-repository ppa:mc3man/trusty-media
RUN apt update; exit 0
RUN apt install -y bc
RUN apt install -y ffmpeg
RUN apt install -y python3.5
RUN apt install -y curl
RUN rm -rf /var/lib/apt/lists/*


RUN curl https://bootstrap.pypa.io/ez_setup.py -o - | python3.5
RUN easy_install-3.5 pip

WORKDIR /app
COPY make_thumbnail.sh /script/make_thumbnail.sh
RUN ln -s /script/make_thumbnail.sh /usr/bin/make_thumbnail

copy requirements.txt .
RUN pip install -r requirements.txt
RUN ln -fs /usr/bin/python3.5 /usr/bin/python3

copy app.py .

ENTRYPOINT [ "./app.py" ]